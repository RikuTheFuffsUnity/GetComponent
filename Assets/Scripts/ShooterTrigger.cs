﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShooterTrigger : MonoBehaviour
{

    public GameObject logShooterObject;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            LogShooter logShooter = logShooterObject.GetComponent<LogShooter>();
            if (logShooter)
            {
                logShooter.score = 10;
                logShooter.ShootLog("Hello");
            }
        }
    }
}
